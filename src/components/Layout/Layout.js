import React from 'react';

import Aux from '../../hoc/Auxilary'
import './Layout.css'

const layout = (props) => (
    <Aux>
        <div>
            Toolbar, SiderDrawer, Backdrop
        </div>
        <main className="Content">
            {props.children}
        </main>
    </Aux>
);

export default layout;