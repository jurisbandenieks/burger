import React from 'react';
import './BuildControls.css';

import BuildControl from './BuildControl/BuildControl';

const controls = [
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Meat', type: 'meat'}
];

const burgerControls = ( props ) => {
    return (
        <div className="BuildControls">
            <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
            {controls.map(ctrl => (
                <BuildControl
                key={ctrl.label}
                label={ctrl.label}
                added={() => props.ingridientAdded(ctrl.type)}
                removed={() => props.ingridientRemoved(ctrl.type)}
                disabled={props.disabled[ctrl.type]}
                />
            ))}
            <button
            className="OrderButton"
            disabled={!props.purchasable}
            onClick={props.ordered}
            >ODER NOW</button>
        </div>
    )  
}

export default burgerControls;